import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';

import {Home} from './screens/Home';
import {Settings} from './screens/Settings';
import {TodoDetail} from './screens/TodoDetail';
import UserContext, {initialUserValue} from './contexts/UserContext';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const HomeTabs = (props) => {
  return (
    <Tab.Navigator tabBarOptions={{style: {backgroundColor: '#3c3836'}}}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Settings" component={Settings} />
    </Tab.Navigator>
  );
};

const HomeDrawer = (props) => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={HomeTabs} />
      <Drawer.Screen name="Settings" component={Settings} />
    </Drawer.Navigator>
  );
};

const App = () => {
  const [user, setUser] = React.useState(initialUserValue);

  return (
    <UserContext.Provider value={{user, setUser}}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="Home"
          screenOptions={({route, navigation}) => {
            return {
              headerStyle: {backgroundColor: '#3c3836'},
              headerTitleStyle: {color: '#f2e5bc'},
            };
          }}>
          <Stack.Screen name="HomeDrawer" component={HomeDrawer} />
          <Stack.Screen name="TodoDetail" component={TodoDetail} />
        </Stack.Navigator>
      </NavigationContainer>
    </UserContext.Provider>
  );
};

export default App;
