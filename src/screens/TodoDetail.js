import React from 'react';

import {SafeAreaView, StyleSheet, Text} from 'react-native';

export const TodoDetail = (props) => {
  const {params} = props.route;
  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.titleStyle}>{params.todo.title}</Text>
      <Text style={styles.descriptionStyle}>{params.todo.description}</Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#282828',
    flex: 1,
    padding: 8,
  },
  titleStyle: {
    color: '#bdae93',
    fontSize: 24,
    fontWeight: '900',
  },
  descriptionStyle: {
    color: '#fbf1c7',
  },
});
