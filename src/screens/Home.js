import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  FlatList,
  Alert,
  TextInput,
  TouchableOpacity,
  Text,
} from 'react-native';

import TodoItem from '../components/TodoItem';
import Header from '../components/Header';
import UserContext from '../contexts/UserContext';
import {usePrevious} from '../hooks/usePrevious';

const initialTodos = [
  {
    title: 'belajar react native components',
    description: 'no description',
    status: 'pending',
  },
  {
    title: 'belajar react hooks',
    description: '-',
    status: 'pending',
  },
];

export const Home = (props) => {
  const [inputValue, setInputValue] = useState('');

  const [todos, setTodos] = useState(initialTodos);

  const handleAddTodo = () => {
    const newTodo = {
      title: inputValue,
      description: '',
      status: 'pending',
    };

    setTodos([...todos, newTodo]);
    setInputValue('');
  };

  const handleRemoveTodo = (todo) => {
    Alert.alert('Remove todo button', `remove item ${todo.title}?`, [
      {
        text: 'no',
      },
      {
        text: 'yes',
        onPress: () =>
          setTodos(todos.filter((_todo) => _todo.title !== todo.title)),
      },
    ]);
  };

  const handleToggleCompletedTodo = (target) => {
    // find a todo by title and update it status
    setTodos(
      todos.map((todo) => {
        if (todo.title === target.title) {
          return {
            ...todo,
            status: todo.status === 'pending' ? 'completed' : 'pending',
          };
        }
        return todo;
      }),
    );
  };

  const handleTodoPress = (todo) => {
    props.navigation.navigate('TodoDetail', {todo});
  };

  const {user, setUser} = React.useContext(UserContext);

  const prevTodosLength = usePrevious(todos.length);
  // perform side-effect when todos change.
  useEffect(() => {
    if (todos.length !== prevTodosLength) {
      Alert.alert('todos updated');
    }
  }, [todos.length, prevTodosLength]);

  useEffect(() => {
    setUser((prevState) => ({...prevState, totalTodo: todos.length}));
  }, [todos.length, setUser]);

  // perform side-effect only once
  useEffect(() => {
    // Alert.alert('screen mounted');
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <Text
        style={{
          color: '#bdae93',
        }}>
        total: {user.totalTodo}
      </Text>
      <Header
        title={`${user.username}'s todos ${
          todos.every((todo) => todo.status === 'completed') ? '🥳' : '😃'
        }`}
      />
      <View style={styles.addTodoContainer}>
        <TextInput
          style={styles.addTodoInput}
          onChangeText={(v) => setInputValue(v)}
          value={inputValue}
        />
        <TouchableOpacity style={styles.addTodoButton} onPress={handleAddTodo}>
          <Text style={styles.addTodoButtonTitle}>Add</Text>
        </TouchableOpacity>
      </View>
      <FlatList
        data={todos}
        keyExtractor={(item) => item.title}
        renderItem={({item}) => (
          <View style={{marginBottom: 16}}>
            <TodoItem
              onPress={() => handleTodoPress(item)}
              onCompleteButtonPress={() => handleToggleCompletedTodo(item)}
              onRemoveButtonPress={() => handleRemoveTodo(item)}
              title={item.title}
              description={item.description}
              status={item.status}
            />
          </View>
        )}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#282828',
    flex: 1,
    padding: 8,
  },
  inputContainer: {},
  addTodoContainer: {
    flexDirection: 'row',
    marginBottom: 16,
  },
  addTodoInput: {
    width: '80%',
    backgroundColor: '#504945',
    color: '#bdae93',
  },
  addTodoButton: {
    width: '20%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#bdae93',
  },
  addTodoButtonTitle: {
    color: '#504945',
    fontWeight: '900',
  },
});
