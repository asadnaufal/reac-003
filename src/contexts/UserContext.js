import React from 'react';

export const initialUserValue = {
  username: 'someone_1',
  totalTodo: 0,
};

const UserContext = React.createContext(initialUserValue);

export default UserContext;
