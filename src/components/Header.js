import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Header = (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.heading}>{props.title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {paddingVertical: 16, paddingHorizontal: 8},
  heading: {fontSize: 32, color: '#f2e5bc', fontWeight: 'bold'},
});

export default Header;
